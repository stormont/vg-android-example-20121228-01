package com.voyagegames.example;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class TextureManager {
	
	public class BundledTexture {
		
		public final Texture       texture;
		public final TextureRegion region;
		
		public BundledTexture(final Texture texture, int offsetX, int offsetY, int width, int height) {
			this.texture = texture;
			this.region = new TextureRegion(this.texture, offsetX, offsetY, width, height);
		}
		
		public float width() {
			return region.getRegionWidth();
		}
		
		public float height() {
			return region.getRegionHeight();
		}
		
	}
	
	private final Map<String, BundledTexture> mDict = new HashMap<String, BundledTexture>();
	
	public void add(final String key, final BundledTexture texture) {
		if (mDict.containsKey(key)) {
			return;
		}
		
		mDict.put(key, texture);
	}
	
	public BundledTexture get(final String key) {
		return mDict.get(key);
	}
	
	public void dispose(final String key) {
		if (!mDict.containsKey(key)) {
			return;
		}
		
		final BundledTexture t = mDict.get(key);
		t.texture.dispose();
		mDict.remove(key);
	}
	
	public void disposeAll() {
		for (final BundledTexture t : mDict.values()) {
			t.texture.dispose();
		}
		
		mDict.clear();
	}

}
